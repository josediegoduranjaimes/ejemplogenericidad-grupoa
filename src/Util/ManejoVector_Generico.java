/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import Modelo.Persona;
import java.util.Arrays;

/**
 *
 * @author madarme- JOSE DURAN 1150817
 * @param <T>
 */
public class ManejoVector_Generico <T extends Comparable<T> > {
    
    private T []vector;

    public ManejoVector_Generico() {
    }

    public ManejoVector_Generico(T[] vector) {
        this.vector = vector;
    }

    public T[] getVector() {
        return vector;
    }

    public void setVector(T[] vector) {
        this.vector = vector;
    }

    @Override
    public String toString() {
       
         String msg="Vector de :"+this.getClass().getSimpleName()+": ";
        for(T datos:this.vector)
        {
            msg+=datos.toString()+"\t";
        }
        
        return msg;
        
        
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
       
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ManejoVector_Generico<T> other = (ManejoVector_Generico<T>) obj;
        
        if(this.vector.length!=other.vector.length)
            return false;
        
        for(int i=0; i<this.vector.length;i++)
        {
            if(! this.vector[i].equals(other.vector[i]))
                return false;
        }
        
        return true;
       
        
        
        
    }
    
    
    /**
     * Ordena el vector en sí mismo usando el método de ordenamiento de inserción, puede tomar como ejemplo la siguiente url:
     * https://juncotic.com/ordenamiento-por-insercion-algoritmos-de-ordenamiento/#:~:text=El%20algoritmo%20de%20ordenamiento%20por%20inserci%C3%B3n%20es%20un%20algoritmo%20de,insert%C3%A1ndolo%20en%20el%20lugar%20correspondiente.
     * 
     * Recomendación: Implementar el método compareTo en la clase Persona 
     */

    public void ordenarInsercion(Persona persona [])
    {
   int tam = this.vector.length;
        int j;
        int aux;
        boolean termino;
        for(int i= 1; i<tam;i++){
            T pivote = this.vector[i];
            termino=false;
            j=i-1;
            aux=i;
            while(!termino){                  
                if(this.vector[j].compareTo(pivote) > 0){                  
                    posicion(this.vector,aux,j);                   
                    aux=j;                  
                }
                if(j==0)termino=true;
                j--;              
            }          
        }
 
        
    }
    
     private void posicion(T arr[], int i, int j) {
        T anterior = arr[i];
        T nuevo = arr[j];
        arr[i] = nuevo;
        arr[j] = anterior;
    }

    
    
    
    
}